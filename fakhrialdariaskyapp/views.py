from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from datetime import datetime, date
import urllib3
import json
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm, AuthenticationForm
from .models import Profile


# Create your views here.
def index(request):
    return render(request, 'main/home.html')

def story7(request):
    return render(request, 'main/story7.html')

def story8(request):
    return render(request, 'main/story8.html')

def api_buku(request):
    http = urllib3.PoolManager()
    response = http.request(
        "GET", "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('q', ''))
    return JsonResponse(json.loads(response.data.decode('utf8')))

def register_view(request):
    if request.user.is_authenticated:
        return redirect("fakhrialdariaskyapp:home")

    context = {
        "register_form" : RegistrationForm
    }

    if request.method == "POST":
        register_form = RegistrationForm(request.POST)
        if register_form.is_valid():
            register_form.save()
            new_user = authenticate(
                username = register_form.cleaned_data['username'],
                password = register_form.cleaned_data['password1']
            )
            new_profile = Profile.objects.create(
                user = new_user,
                full_name = new_user.username
            )
            login(request, new_user)

            return redirect("profile_view", username=new_user.username)
        elif register_form.errors:
            context["error"] = "Please enter valid a name and password."
    return render(request, "main/story9_register.html", context)


def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("fakhrialdariaskyapp:home")

def login_view(request):
    context = {
        "login_form" : LoginForm
    }

    if request.user.is_authenticated:
        return redirect("fakhrialdariaskyapp:home")

    if request.method == "POST":
        login_form = LoginForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login(request, user)
            print(user.username)
            return redirect("fakhrialdariaskyapp:profile_view",username=user.username)
        elif login_form.errors:
            context["error"] = "Invalid Username or Password."
    return render(request, "main/story9_login.html", context)

def profile_view(request, username):
    user = User.objects.get(username = username)
    context = {
        "profile" : Profile.objects.get(user = user)
    }
    return render(request, "main/story9_profile.html", context)




