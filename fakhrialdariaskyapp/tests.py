from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
import json
# Create your tests here.

class UnitTest(TestCase):
    def test_GET_homepage(self):
        response = self.client.get(reverse('fakhrialdariaskyapp:home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"main/home.html")

    def test_GET_story7(self):
        response = self.client.get(reverse('fakhrialdariaskyapp:story7'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"main/story7.html")

    def test_GET_story8(self):
        response = self.client.get(reverse('fakhrialdariaskyapp:story8'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"main/story8.html")

    def test_GET_API_page(self):
        response = self.client.get(reverse('fakhrialdariaskyapp:api_buku') + '?q=Math')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Math', response.content.decode('utf-8'))

    def test_GET_login(self):
        response = self.client.get(reverse('fakhrialdariaskyapp:login_view'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"main/story9_login.html")

    def test_GET_register(self):
        response = self.client.get(reverse('fakhrialdariaskyapp:register_view'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"main/story9_register.html")


