from django.db import models
from django.shortcuts import reverse
'''module for modeling the Post'''
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

#User
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.TextField()
    profile_picture_url = models.URLField(blank = True, default = "https://iupac.org/wp-content/uploads/2018/05/default-avatar-300x300.png")
    description = models.TextField(blank = True, default = "Hi! Ini profil kamu.")

    def __str__(self):
        return f"Profil {self.user.username}"
