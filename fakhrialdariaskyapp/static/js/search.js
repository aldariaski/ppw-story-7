$(document).ready(function() {
    //$('#search').click(function(event) {
    $("#search-query").on("keyup", function(e) {
        //event.preventDefault();
        $('#hasilSearch').innerHTML = "";
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + $('#search-query').val(),
            success: printHasil
        });
    })
})

function printHasil(hasilSearch) {
    let json = hasilSearch.items;
    let inner = "";
    for (let i = 0; i < json.length; i++) {
        let data = json[i].volumeInfo;
        inner +=
            `
<div class="card text-dark w-75 mx-auto my-4">
      <div class="card-header bg-info text-light text-center text-left-lg">
      <h3>${data.title}</h3> 
      <p class="lead">(oleh ${(data.authors === undefined ? "Anonim" : data.authors)})</p>
      </div>
      
      <div class="card-body bg-muted">
        <div class="row">
          <div class="col-lg-3 col-12 d-flex align-items-center justify-content-center">
            <img class="mr-3 book-cover" src="${(data.imageLinks === undefined ? "https://www.freeiconspng.com/img/23485" : data.imageLinks.thumbnail)}">
          </div>
          <div class="col-lg-9 col-12 book-info">
            <h5 class="card-title">${(data.publishedDate === undefined ? "Tanggal terbit tidak diketahui" : "Diterbitkan pada " + data.publishedDate)}</h5>
            <p class="card-text text-justify " style="overflow: auto;text-overflow: ellipsis; max-height: 10rem;">${data.description ? data.description : "Deskripsi tidak tersedia"}</p>
          </div>
        </div>
      </div>
    </div>
`
    }

    $('#hasilSearch').html(inner); //Print ke htmlnya

}