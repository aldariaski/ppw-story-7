from django.urls import path

from . import views

app_name = 'fakhrialdariaskyapp'

urlpatterns = [
    path('', views.index, name='home'),
    path('story7', views.story7, name='story7'),
    path('story8', views.story8, name='story8'),
    path('api_buku', views.api_buku, name='api_buku'),
    path("login/", views.login_view, name = "login_view"),
    path("register/", views.register_view, name = 'register_view'),
    path("logout/", views.logout_view, name = "logout_view"),
    path("profile/<str:username>/", views.profile_view, name = "profile_view"),

]
